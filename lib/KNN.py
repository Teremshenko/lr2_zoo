import numpy as np
from operator import itemgetter
from scipy.spatial import distance
import pandas as pd


class KNNgeneral:
    def __init__(self, data, test, k, classColumnName, metricNumber):
        self.data = data
        self.test = test
        self.k = k
        self.classColumnName = classColumnName
        self.metricNumber = metricNumber

    def getDistEuclidean(self, vector1, vector2):
        return distance.euclidean(vector1, vector2)

    def getAccurancy(self, resultDataFrame, actualResultColumnName, predictedResultColumnName):
        counter = 0
        length = len(resultDataFrame)
        for i in range(length):
            if resultDataFrame.iloc[i][actualResultColumnName] != resultDataFrame.iloc[i][predictedResultColumnName]:
                counter = counter + 1
        return 1 - (counter / length)

    def Normalization(norm):
        maxN = norm.max()
        minN = norm.min()
        norm2 = []

        ii = 0
        while ii < norm.size:
            z = (norm[ii] - minN) / (maxN - minN)
            norm2.append(z)
            ii = ii + 1
        norm = norm2
        return norm

    def generalCalculations(self):
        y_data = self.data[self.classColumnName]
        data = self.data.drop([self.classColumnName], axis=1)
        kNeighboursForTest = []
        for test_index, test_row in self.test.iterrows():
            labels = []
            testDistances = {}


            if self.metricNumber == 1:
                for data_index, data_row in data.iterrows():
                    testDistances[data_index] = self.getDistEuclidean(test_row, data_row)
            else:
                raise NameError('Please, choose proper type of metric: 1 - Euclidean, 2 - other (no realisation)')
            testDistancesSorted = (sorted(testDistances.items(), key=itemgetter(1)))

            for i in range(0, self.k):
                labels.append(y_data[testDistancesSorted[i][0]])
            kNeighboursForTest.append(labels)
        return kNeighboursForTest


class KNNclassifier(KNNgeneral):
    def __init__(self, data, test, k, classColumnName, metricNumber):
        return KNNgeneral.__init__(self, data, test, k, classColumnName, metricNumber)

    def predict(self):
        kNeighboursForTest = self.generalCalculations()
        length = len(kNeighboursForTest)
        labeled_test = []
        for i in range(length):
            a_set = set(kNeighboursForTest[i])

            most_common = None  # наиболее часто встречаемое значение
            qty_most_common = 0  # его количество

            # находим самый частовстречаемый класс
            for item in a_set:
                qty = kNeighboursForTest[i].count(item)
                if qty > qty_most_common:
                    qty_most_common = qty
                    most_common = item
                    # записываем найденный класс в результирующий список
            labeled_test.append(most_common)

        return pd.DataFrame(labeled_test)


class KNNregression(KNNgeneral):
    def __init__(self, data, test, k, classColumnName, metricNumber):
        return KNNgeneral.__init__(self, data, test, k, classColumnName, metricNumber)

    def predict(self):
        kNeighboursForTest = self.generalCalculations()
        length = len(kNeighboursForTest)
        labeled_test = []
        for i in range(length):
            avg_val = np.mean(kNeighboursForTest[i])
            labeled_test.append(avg_val)

        return pd.DataFrame(list(map(round, labeled_test)))
